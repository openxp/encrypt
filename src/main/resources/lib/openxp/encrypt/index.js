var encryptBean = __.newBean('openxp.lib.encrypt.Encrypt');

var encrypt = function(plainText, password){
    return encryptBean.encrypt(plainText, password);
}

var decrypt = function(encyptedText, password){
    return encryptBean.decrypt(encyptedText, password);
}

exports.encrypt = encrypt;
exports.decrypt = decrypt;