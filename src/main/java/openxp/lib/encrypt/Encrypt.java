package openxp.lib.encrypt;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Encrypt {
    private static final Logger LOGGER = Logger.getLogger(Encrypt.class.getName());

    public static SecretKey generateSecretKey(String password, byte[] iv) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), iv, 65536, 128); // AES-128
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] key = secretKeyFactory.generateSecret(spec).getEncoded();
        return new SecretKeySpec(key, "AES");
    }

    public byte[] encrypt(String plainText, String password) throws InvalidKeySpecException {
        try {

            // GENERATE random nonce (number used once)
            SecureRandom random = SecureRandom.getInstanceStrong();
            final byte[] iv = new byte[12];
            random.nextBytes(iv);

            //Prepare your key/password
            SecretKey secretKey = generateSecretKey(password, iv);

            // ENCRYPTION
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            GCMParameterSpec spec = new GCMParameterSpec(128, iv);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, spec);

            byte[] encryptedData = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));

            //Concatenate everything and return the final data
            ByteBuffer byteBuffer = ByteBuffer.allocate(4 + iv.length + encryptedData.length);
            byteBuffer.putInt(iv.length);
            byteBuffer.put(iv);
            byteBuffer.put(encryptedData);

            return byteBuffer.array();

        } catch (InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidParameterException e) {
            LOGGER.log(Level.SEVERE, e.getLocalizedMessage());
            return null;
        }
    }

    public String decrypt(byte[] encryptedData, String password) throws InvalidKeySpecException {
        try {

            //Wrap the data into a byte buffer to ease the reading process
            ByteBuffer byteBuffer = ByteBuffer.wrap(encryptedData);
            int noonceSize = byteBuffer.getInt();

            //Make sure that the file was encrypted properly
            if (noonceSize < 12 || noonceSize >= 16) {
                throw new IllegalArgumentException("Nonce size is incorrect. Make sure that the incoming data is an AES encrypted file.");
            }
            byte[] iv = new byte[noonceSize];
            byteBuffer.get(iv);

            //Prepare your key/password
            SecretKey secretKey = generateSecretKey(password, iv);

            //get the rest of encrypted data
            byte[] cipherBytes = new byte[byteBuffer.remaining()];
            byteBuffer.get(cipherBytes);

            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);

            //Encryption mode on!
            cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);

            //Encrypt the data
            byte[] decryptedBytes = cipher.doFinal(cipherBytes);
            String plainText = new String(decryptedBytes);
            return plainText;

        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | InvalidParameterException | InvalidAlgorithmParameterException e) {
            LOGGER.log(Level.SEVERE, e.getLocalizedMessage());
            return null;
        }
    }

}