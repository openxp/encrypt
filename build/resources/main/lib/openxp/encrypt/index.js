var encryptBean = __.newBean('openxp.lib.encrypt.Encrypt');
//var appersist = require('/lib/openxp/appersist');

/*var generateKey = function(password){
    return encryptBean.generateKey(password);
}

var persistKey = function(keyByteArray){
    appersist.repository.getConnection({repository:'openxp.lib.encrypt', branch:'draft'}).create({key:keyByteArray});
}*/

var encrypt = function(plainText, password){
    return encryptBean.encrypt(plainText, password);
}

var decrypt = function(encyptedText, password){
    return encryptBean.decrypt(encyptedText, password);
}

//exports.generateKey = generateKey;
//exports.persistKey = persistKey;
exports.encrypt = encrypt;
exports.decrypt = decrypt;